///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file hex_dump.c
/// @version 1.0
///
/// Print output for the -x file header option
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "readelf.h"
#include "hex_dump.h"
#include "util.h"


// To process the -x option...
//  Build the linked list of -x names... It'll be built LIFO
//  Iterate over the list... If the name is not in the list, then print a message
//  Iterate over the list... if the name matches, then print the section

// $ readelf -x .init -x .fini -x blob -x blob2 readelf readelf


// Hold the parsed -x parameter.
//
// if name == NULL, then index is valid and name is invalid
// If name != NULL, then name is valid and index is invalid
typedef struct Dump_section_info {
	const char*               name;  // Pointer directly into argv.
	uint16_t                  index; // 0, 1, 2, ...
	struct Dump_section_info* next;  // Pointer to the next node
} Dump_section_info;

Dump_section_info* dump_section_head = NULL;


// Add a section name (or index) to the linked list
void addSectionName( const char* name ) {

	Dump_section_info* newInfo = malloc( sizeof( Dump_section_info ));

	if( isNumber( name )) {
		newInfo->name = NULL;
		newInfo->index = atoi( name );
		// printf( "Section index [%"PRId16"]\n", newInfo->index );
	} else {
		newInfo->name = name;
		newInfo->index = 0;
		// printf( "Section name [%s]\n", newInfo->name );
	}

	newInfo->next = dump_section_head;
	dump_section_head = newInfo;
}


void cleanupSectionNameList() {
	Dump_section_info* node = dump_section_head;

	while( node != NULL ) {
		Dump_section_info* nextNode = node->next;
		// printf( "Deleting [%s]\n", node->name );
		free( node );
		node = nextNode;
	}

	dump_section_head = NULL;
}


// Look for any -x <LABEL> that isn't actually in the program
// and print a message if the user is asking for something
// that isn't there.
void checkSections ( const char* filename ) {
	// Search for -x names... if there is no section that has a hit, then print:
	// readelf: readelf: Warning: Section 'foo' was not dumped because it does not exist!
	Dump_section_info* xNode = dump_section_head;

	while( xNode != NULL ) { // For each -x node
		Elf_section_node* sectionNode = elf_section_list_head;
		bool foundSectionNode = false;

		while( sectionNode != NULL ) { // For each section node
			if( xNode->name == NULL ) { // This is an index node...
				if( sectionNode->index == xNode->index ) {
					// printf( "Found index [%d]\n", xNode->index );
					foundSectionNode = true;
					break;
				}
			} else {                    // This is a name node...
				if( strcmp( sectionNode->name, xNode->name ) == 0 ) {
					// printf( "Found name [%s]\n", xNode->name );
					foundSectionNode = true;
					break;
				}

			}
			sectionNode = sectionNode->next;
		}

		// If we don't find a node, then print a message
		if( foundSectionNode == false ) {
			if( xNode->name == NULL ) {
				printf( "%s: %s: Warning: Section '%d' was not dumped because it does not exist!\n", PROGRAM_NAME, filename, xNode->index );
			} else {
				printf( "%s: %s: Warning: Section '%s' was not dumped because it does not exist!\n", PROGRAM_NAME, filename, xNode->name );
			}
		}

		xNode = xNode->next;
	}
}


bool isAscii( unsigned char c ) {
	if( c >= 0x00 && c <= 0x1f )
		return false;
	if( c >= 0x20 && c <= 0x7e)
		return true;
//	if( c >= 0x7f )
		return false;
}


// Hex dump an ELF section.  Example output:
//
// Hex dump of section '.interp':
//   0x004002a8 2f6c6962 36342f6c 642d6c69 6e75782d /lib64/ld-linux-
//   0x004002b8 7838362d 36342e73 6f2e3200          x86-64.so.2.
//
void dumpSection( Elf_section_node* section, void* buf ) {
	uint64_t addr   ;
	uint64_t offset ;
	uint64_t size   ;

	if( addressSize == ELF64 ) {
		addr   = fixup64( section->elf_section->sh_addr   ) ;
		offset = fixup64( section->elf_section->sh_offset ) ;
		size   = fixup64( section->elf_section->sh_size   ) ;
	} else {        // ELF32
		addr   = fixup32( ((Elf_section32*)section->elf_section)->sh_addr   ) ;
		offset = fixup32( ((Elf_section32*)section->elf_section)->sh_offset ) ;
		size   = fixup32( ((Elf_section32*)section->elf_section)->sh_size   ) ;
	}
	uint64_t hexIndex   = 0;
	uint64_t asciiIndex = 0;

	printf( "\n" );
	printf( "Hex dump of section '%s':\n", section->name );

	while( hexIndex < size ) {
		// Print address
		printf( "  0x%08"PRIx64" ", addr + hexIndex );

		// Print hex values in blocks of 16
		for( uint64_t column = 0 ; column < 16 ; column++ ) {
			if( hexIndex < size ) {
				uint8_t x = *(unsigned char*)(buf + offset + hexIndex);
				printf( "%02x", x );
			} else {
				printf( "  " );
			}
			if( (column % 4) == 3 ) {
				printf( " " );
			}
			hexIndex++;
		}

		// Print ascii characters in blocks of 16
		for( uint64_t column = 0 ; column < 16 ; column++ ) {
			if( asciiIndex < size ) {
				char x = *(char*)(buf + offset + asciiIndex);
				if( isAscii( x )) {
					printf( "%c", x );
				} else {
					printf( "." );
				}
			}
			asciiIndex++;
		}

		printf( "\n" );
	}
	printf( "\n" );
}


void dumpSections ( void* buf ) {
	Elf_section_node* sectionNode = elf_section_list_head;

	while( sectionNode != NULL ) { // For each section node
		bool foundSectionNode = false;
		Dump_section_info* xNode = dump_section_head;

		while( xNode != NULL ) {    // For each -x node

			if( xNode->name == NULL ) { // This is an index node...
				if( sectionNode->index == xNode->index ) {
					// printf( "Found index [%d]\n", xNode->index );
					foundSectionNode = true;
					break;
				}
			} else {                    // This is a name node...
				if( strcmp( sectionNode->name, xNode->name ) == 0 ) {
					// printf( "Found name [%s]\n", xNode->name );
					foundSectionNode = true;
					break;
				}

			}

			xNode = xNode->next;
		}

		if( foundSectionNode == true ) {
			dumpSection( sectionNode, buf );
		}

		sectionNode = sectionNode->next;
	}
}


void hexDumpSections( const char* filename, void* buf ) {
	checkSections( filename );

	dumpSections( buf );
}
