///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file hex_dump.h
/// @version 1.0
///
/// Print output for the -x file header option
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

void addSectionName( const char* name );
void hexDumpSections( const char* filename, void* buf );
void cleanupSectionNameList();
