///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file util.c
/// @version 1.0 - Initial implementation of -h option
/// @version 2.0 - Refactored.  Added -t and -x options.
///
/// Utilities for the readelf program
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <inttypes.h>
#include <string.h>
#include <ctype.h>

#include "readelf.h"
#include "util.h"


/// Check if the elf file is a 32 or 64 bit elf program
void check32or64bit() {
	const unsigned char class = elf_header.e_magic.elf_magic.e_class;

	const unsigned char ELFCLASS32 = 0x01;
	const unsigned char ELFCLASS64 = 0x02;

	if(      (class & ELFCLASS32) != 0 ) {
		addressSize = ELF32;
	}
	else if( (class & ELFCLASS64) != 0 ) {
		addressSize = ELF64;
	}
	else {
		addressSize = ADDRESS_SIZE_UNKNOWN;
	}
}


/// Check the endianess of readelf (this program)
void checkProgramEndianess() {
	int n = 1;

	if( *(char *)&n == 1 ) {  // little endian if true
		programEndianess = LITTLE;
	} else {
		programEndianess = BIG;
	}
}


/// Check the endianess of the current elf file, ...
///
/// Finally, if the endianess of the current elf file
/// is the same as the file we are reading, then
/// set isSameEndianess to true.
void checkElfEndianess() {
	// Check the endianess of the elf file
	const unsigned char endianess = elf_header.e_magic.elf_magic.e_endianess;

	const unsigned char ELFDATA2LSB = 0x01;
	const unsigned char ELFDATA2MSB = 0x02;

	if(      (endianess & ELFDATA2LSB) != 0 ) {
		elfEndianess = LITTLE;
	}
	else if( (endianess & ELFDATA2MSB) != 0 ) {
		elfEndianess = BIG;
	}
	else {
		elfEndianess = ENDIAN_UNKNOWN;
	}

	// Compare elf endianess with this program...
	if( elfEndianess == programEndianess ) {
		isSameEndianess = true;
	} else {
		isSameEndianess = false;
	}
}


/// Copy size bytes from the 'from' pointer to the 'to' pointer.
/// Increment the 'from' pointer by the number of bytes copied.
void parseUnsignedCharArray( void* to, void** from, size_t size ) {
	for( size_t i = 0 ; i < size ; i++ ) {
		*(unsigned char*)(to + i) = *((unsigned char*)*(from)+i);
	}

	*from += size;  // Increment the from pointer by size bytes
}


/// Swap the 2 bytes of a uint16_t
uint16_t swap16( uint16_t swap ) {

#if defined(__i386__) || defined(__x86_64__)

	// Use inline assembly to swap the bytes
	asm volatile ( "XCHG %h0, %b0" : "+a" (swap) );

	return( swap );

#else

	// The following code is necessary on non-x86 machines
	union {
		uint16_t      machineWord;
		unsigned char bytes[2];
	} overlay;

	overlay.machineWord = swap;

	unsigned char temp;
	temp             = overlay.bytes[0];
	overlay.bytes[0] = overlay.bytes[1];
	overlay.bytes[1] = temp;

	return overlay.machineWord;

#endif
}


/// Fixup the 2 bytes of a uint16_t
uint16_t fixup16( uint16_t from ) {
	if( ! isSameEndianess ) {
		return swap16( from ) ;
	}
	return from;
}


/// Copy 2 bytes from the 'from' pointer to the 'to' pointer into
/// a uint16_t.  If not isSameEndianess, then fixup the endianess
///
/// Increment the 'from' pointer by the number of bytes copied.
void parseUint16( void* to, void** from ) {
	*(uint16_t*)to = fixup16( *(uint16_t*)*from );

	*from += 2;  // Increment offset by 2 bytes
}


/// Swap the 4 bytes of a uint32_t
uint32_t swap32( uint32_t swap ) {

#if defined(__i386__) || defined(__x86_64__)

	// Use inline assembly to swap the bytes
	asm volatile ( "BSWAP %[rSwap]" : [rSwap] "+r" (swap) : );

	return( swap );

#else

	// The following code is necessary on non-x86 machines
	union {
		uint32_t      machineWord;
		unsigned char bytes[4];
	} overlay;

	overlay.machineWord = swap;

	unsigned char temp;
	temp             = overlay.bytes[0];
	overlay.bytes[0] = overlay.bytes[3];
	overlay.bytes[3] = temp;

	temp             = overlay.bytes[1];
	overlay.bytes[1] = overlay.bytes[2];
	overlay.bytes[2] = temp;

	return overlay.machineWord;

#endif
}


/// Fixup the 4 bytes of a uint32_t
uint32_t fixup32( uint32_t from ) {
	if( ! isSameEndianess ) {
		return swap32( from ) ;
	}
	return from;
}


/// Copy 4 bytes from the 'from' pointer to the 'to' pointer into
/// a uint32_t.  If not isSameEndianess, then fixup the endianess
///
/// Increment the 'from' pointer by the number of bytes copied.
void parseUint32( void* to, void** from ) {
	*(uint32_t*)to = fixup32( *(uint32_t*)*from );

	*from += 4;  // Increment offset by 4 bytes
}


/// Swap the 8 bytes of a uint64_t
uint64_t swap64( uint64_t swap ) {

#if defined(__x86_64__)

	// #pragma message __FILE__": Using inline assembly."

	// Use inline assembly to swap the bytes
	asm volatile ( "BSWAP %[rSwap]" : [rSwap] "+r" (swap) : );

	return( swap );

#else

	// The following code is necessary on non-x86 machines
	union {
		uint64_t      machineWord;
		unsigned char bytes[8];
	} overlay;

	overlay.machineWord = swap;

	unsigned char temp;
	temp             = overlay.bytes[0];
	overlay.bytes[0] = overlay.bytes[7];
	overlay.bytes[7] = temp;

	temp             = overlay.bytes[1];
	overlay.bytes[1] = overlay.bytes[6];
	overlay.bytes[6] = temp;

	temp             = overlay.bytes[2];
	overlay.bytes[2] = overlay.bytes[5];
	overlay.bytes[5] = temp;

	temp             = overlay.bytes[3];
	overlay.bytes[3] = overlay.bytes[4];
	overlay.bytes[4] = temp;

	return overlay.machineWord;

#endif
}


/// Fixup the 8 bytes of a uint64_t
uint64_t fixup64( uint64_t from ) {
	if( ! isSameEndianess ) {
		return swap64( from ) ;
	}
	return from;
}


/// Copy 8 bytes from the 'from' pointer to the 'to' pointer into
/// a uint64_t.  If not isSameEndianess, then fixup the endianess.
///
/// Increment the 'from' pointer by the number of bytes copied.
void parseUint64( void* to, void** from ) {
	*(uint64_t*)to = fixup64( *(uint64_t*)*from );

	*from += 8;  // Increment offset by 8 bytes
}


// Return true if str is an integer
bool isNumber( const char* str ) {
	int len = strlen( str );

	for( int i = 0 ; i < len ; i++ ) {
		if( !isdigit( str[i] )) {
			return false;
		}
	}

	return true;  // Everything is a digit
}
