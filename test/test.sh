###############################################################################
# University of Hawaii, College of Engineering
# EE 491F - Software Reverse Engineering
# Lab 05c - Readelf
#
# Comprehensive regression test for readelf
#
# Uses /bin/readelf as a reference.  Need to remove the flags as we are not
# parsing them.
#
# @file    test.sh
# @version 1.0 - Initial implementation of -h option
# @version 2.0 - Refactored.  Added -t and -x options.
#
# @author Mark Nelson <marknels@hawaii.edu>
# @brief  Lab 05b - Readelf - EE 491F - Spr 2021
# @date   22 Feb 2021
###############################################################################


WORKING_DIR=./tmp

rm -fr $WORKING_DIR
mkdir  $WORKING_DIR

for TEST in test1 test2 test3 test4 test5 test6 test7
do
	# Test the -h option
	head -21 $TEST.-h.out > $WORKING_DIR/reference
	../readelf -h $TEST > $WORKING_DIR/sample
	diff $WORKING_DIR/reference $WORKING_DIR/sample

	# Test the -t option
	cat $TEST.-t.out > $WORKING_DIR/reference
	../readelf -t $TEST > $WORKING_DIR/sample
	diff $WORKING_DIR/reference $WORKING_DIR/sample

	# Test the -x option
	echo $TEST
	readelf    -x 1 -x 2 -x .text $TEST > $WORKING_DIR/reference
	../readelf -x 1 -x 2 -x .text $TEST > $WORKING_DIR/sample
	diff $WORKING_DIR/reference $WORKING_DIR/sample
done

# Test multi-file -h option
cat all.-h.out > $WORKING_DIR/reference
../readelf -h test1 test2 test3 test4 test5 test6 test7 > $WORKING_DIR/sample
diff $WORKING_DIR/reference $WORKING_DIR/sample

# Test multi-file -t option
# Created by:
# $ readelf -t test1 test2 test3 test4 test5 test6 test7 > all.-t.out
# ... and then deleting the Flags
cat all.-t.out > $WORKING_DIR/reference
../readelf -t test1 test2 test3 test4 test5 test6 test7 > $WORKING_DIR/sample
diff $WORKING_DIR/reference $WORKING_DIR/sample

# Test multi-file *and* multi--x option
readelf    -x 1 -x 2 -x .data test1 test2 test3 test4 test5 test6 test7 > $WORKING_DIR/reference
../readelf -x 1 -x 2 -x .data test1 test2 test3 test4 test5 test6 test7 > $WORKING_DIR/sample
diff $WORKING_DIR/reference $WORKING_DIR/sample

rm -fr $WORKING_DIR
