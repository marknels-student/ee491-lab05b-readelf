///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file util.h
/// @version 1.0 - Initial implementation of -h option
/// @version 2.0 - Refactored.  Added -t and -x options.
///
/// Utilities for the readelf program
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <inttypes.h>
#include <stddef.h>
#include <stdbool.h>

void checkProgramEndianess();
void checkElfEndianess() ;
void check32or64bit() ;

uint16_t swap16( uint16_t swap );
uint32_t swap32( uint32_t swap );
uint64_t swap64( uint64_t swap );

uint16_t fixup16( uint16_t from );
uint32_t fixup32( uint32_t from );
uint64_t fixup64( uint64_t from );

void parseUnsignedCharArray( void* to, void** from, size_t size );
void parseUint16( void* to, void** from );
void parseUint32( void* to, void** from );
void parseUint64( void* to, void** from );

bool isNumber( const char* str );
