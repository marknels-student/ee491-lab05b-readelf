///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file readelf.c
/// @version 1.0 - Initial implementation of -h option
/// @version 2.0 - Refactored.  Added -t and -x options.
///
/// Display information about ELF files
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

// @todo Rename file_header to print_elf_header
// @todo Improve the byte swapper
// @todo Support multiple -x options

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <inttypes.h>

#include "readelf.h"
#include "util.h"
#include "parse_file.h"
#include "print_header.h"
#include "print_sections.h"
#include "hex_dump.h"


////////////////////////////                      ////////////////////////////
////////////////////////////  Global Definitions  ////////////////////////////
////////////////////////////                      ////////////////////////////

const char    PROGRAM_NAME[]  = "readelf";
const uint8_t  ELF_VERSION    = 1;

Address_Size addressSize      = ADDRESS_SIZE_UNKNOWN;
Endian       elfEndianess     = ENDIAN_UNKNOWN;
Endian       programEndianess = ENDIAN_UNKNOWN;
bool         isSameEndianess;            /// Set to true if both the elf and
													  /// this program have the same endianess
Elf_section_node* ptrToTextSectionNode;  /// Point to the node that holds a
                                         /// string table

/// Stores the entire elf header data structure
Elf_header elf_header;

/// Pointer to the head of a linked list of elf section headers
Elf_section_node* elf_section_list_head;

/// Stores all of the command line option settings
struct Program_Options {
	bool displayHeader;          // -h Display the ELF Header Files
	bool displaySectionDetails;  // -t Display the section details
	bool displayHexDump;         // -x Display a hex dump of sections
} programOptions;



//////////////////////////////                   //////////////////////////////
//////////////////////////////  Local Functions  //////////////////////////////
//////////////////////////////                   //////////////////////////////

/// Print the usage statement
void printUsage() {
	printf( "Usage: readelf <option(s)> elf-file(s)\n" );
	printf( " Display information about the contents of ELF format files\n" );
	printf( " Options are:\n" );
	printf( "  -h  Display the ELF file header\n" );
	printf( "  -t  Displays the detailed section information\n" );
	printf( "  -x <number or name>  Displays the contents of the indicated section as a hexadecimal bytes\n" );
}


/// Process filename
///
/// Memory map the file, which saves quite a bit of reading, mallocing
/// and copying data.
void processFile( const char* filename ) {
	int file = open( filename, O_RDONLY );
	if ( file == -1 ) {
		printf( "%s: %s: Error: No such file\n", PROGRAM_NAME, filename );
		return;
	}

	struct stat fileStatus;

	if ( fstat( file, &fileStatus ) != 0 ) {
		printf( "%s: %s: Error: Unable to get file size\n", PROGRAM_NAME, filename );
		return;
	}

	void* buf = mmap( NULL, fileStatus.st_size, PROT_READ, MAP_PRIVATE, file, 0 );
	if( buf == MAP_FAILED ) {
		printf( "%s: %s: Error: Unable to map file\n", PROGRAM_NAME, filename );
		return;
	}

	parse_file( buf );  /// Parse the file into the elf_header data structure

	/// Print output
	if( programOptions.displayHeader ) {
		printElfFileHeader();
	}

	if( programOptions.displaySectionDetails ) {
		printElfFileSectionDetail();
	}

	if( programOptions.displayHexDump ) {
		hexDumpSections( filename, buf );  /// Need filename for error messages
	}

	cleanupSectionList();

	munmap( buf, fileStatus.st_size );
	close( file );
}


/// Main entry point for readelf
///
/// Process options
/// Process filenames passed in from the command line
int main(int argc, char* argv[]) {
	checkProgramEndianess();
	memset ( &programOptions, 0, sizeof( programOptions ));  // Set all options to false

	char c;

	while ((c = getopt (argc, argv, "htx:")) != -1) {
		switch (c) {
			case 'h':
				programOptions.displayHeader   = true;
				break;
			case 't':
				programOptions.displaySectionDetails = true;
				break;
			case 'x':
				programOptions.displayHexDump = true;
				addSectionName( optarg );
				break;
			case '?':
				fprintf (stderr, "Unknown option character '\\x%x'.\n", optopt);
				printUsage();
				exit( EXIT_FAILURE );
				break;
		}
	}


	// At this point, we've processed the switches and it's time to process the
	// filenames.  Fortunately, readelf does not process from stdin.
	//
	// The program can have 1 or more filenames as input.  If there's only one
	// file, then the filename is not printed.  If there are multiple files, then
	// each "File: %s" is printed before processing.
	//
	// At this point, optind -- the Option Index -- is pointing to the first filename
	// and argc -- the Argument Count -- is just past the end.
	//   - If you have no filenames, they will be the same.
	//   - If you have 1 filename, optind will be one less than argc.
	//   - If you have multiple filenames, optind and argc will be separated by 2 or more.
	//

	// printf( "optind = [%d]   argc - [%d]\n", optind, argc );

	// No files on the command line
	if ( optind >= argc ) {
		printUsage() ;
		return( EXIT_FAILURE );
	}

	// There's only one file to process
	else if ( optind + 1 == argc ) {
		processFile( argv[optind] );
	}

	// At this point, we know we have multiple files
	else {
		for (int i = optind; i < argc; i++) {
			printf( "\n" );
			printf( "File: %s\n", argv[i] );

			processFile( argv[i] );
		}
	}

	cleanupSectionNameList();

	return( EXIT_SUCCESS );
}
