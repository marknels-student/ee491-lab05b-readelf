###############################################################################
# University of Hawaii, College of Engineering
# EE 491F - Software Reverse Engineering
# Lab 05c - Readelf
#
# @file    Makefile
# @version 1.0 - Initial implementation of -h option
# @version 2.0 - Refactored.  Added -t and -x options.
#
# @author Mark Nelson <marknels@hawaii.edu>
# @brief  Lab 05b - Readelf - EE 491F - Spr 2021
# @date   22 Feb 2021
###############################################################################

CC=gcc
CFLAGS=-Wall -Wno-unused-variable

all: readelf

util.o: util.c util.h readelf.h
	$(CC) -c $(CFLAGS) util.c

readelf.o: readelf.c readelf.h util.h parse_file.h print_header.h print_sections.h
	$(CC) -c $(CFLAGS) readelf.c

print_header.o: print_header.c print_header.h readelf.h
	$(CC) -c $(CFLAGS) print_header.c

parse_file.o: parse_file.c parse_file.h readelf.h util.h
	$(CC) -c $(CFLAGS) parse_file.c

print_sections.o: print_sections.c print_sections.h readelf.h util.h
	$(CC) -c $(CFLAGS) print_sections.c

hex_dump.o: hex_dump.c hex_dump.h readelf.h
	$(CC) -c $(CFLAGS) hex_dump.c

readelf:  readelf.o util.o print_header.o parse_file.o print_sections.o hex_dump.o
	$(CC) $(CFLAGS) -o $@ $^

test: readelf
	cd test ; ./test.sh

clean:
	rm -f *.o readelf
