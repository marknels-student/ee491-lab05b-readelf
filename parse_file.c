///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file parse_file.c
/// @version 1.0 - Initial implementation of -h option
/// @version 2.0 - Refactored.  Added -t and -x options.
///
/// Parse the elf file into a structure that can be read by the other modules
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdlib.h>

#include "readelf.h"
#include "parse_file.h"
#include "util.h"


static const uint16_t SHN_UNDEF = 0;  // Undefined section


/// Parse the elf header.
///
/// 32 and 64 bit elf headers are variable length around 3 fields...
/// offset points to the current location of the parser.  It is passed
/// by reference into each of the parsers and the parser advances it
/// the appropriate number of bits.
///
/// The parsers are setup as:  parseThing( to, from );
///   Generally, to points to a field in elf_header
///   From is the address of the offset pointer
///   Byte ordering gets fixed up as they data is parsed and copied
void parse_file( void* buf ) {
	// Initialize variables that change from file-to-file (when
	// processing multiple files)
	memset( &elf_header, 0, sizeof( elf_header ));
	elfEndianess           = ENDIAN_UNKNOWN;
	addressSize            = ADDRESS_SIZE_UNKNOWN;
	ptrToTextSectionNode   = NULL;


	/////////////////////////                        /////////////////////////
	/////////////////////////  Parse the ELF Header  /////////////////////////
	/////////////////////////                        /////////////////////////

	// Points to the current location in the memory mapped file (buf)
	void* offset = buf;

	parseUnsignedCharArray( &elf_header.e_magic.e_ident, &offset, SIZE_OF_IDENT_REGION );
	check32or64bit();
	checkElfEndianess();
	parseUint16 ( &elf_header.e_type                         , &offset );
	parseUint16 ( &elf_header.e_machine                      , &offset );
	parseUint32 ( &elf_header.e_version                      , &offset );
	if( addressSize == ELF32 ) {
		parseUint32( &elf_header.e_entry                      , &offset );
		parseUint32( &elf_header.e_program_header_table_offset, &offset );
		parseUint32( &elf_header.e_section_header_table_offset, &offset );
	} else {
		parseUint64( &elf_header.e_entry                      , &offset );
		parseUint64( &elf_header.e_program_header_table_offset, &offset );
		parseUint64( &elf_header.e_section_header_table_offset, &offset );
	}
	parseUint32 ( &elf_header.e_flags                        , &offset );
	parseUint16 ( &elf_header.e_elf_header_size              , &offset );
	parseUint16 ( &elf_header.e_program_header_entry_size    , &offset );
	parseUint16 ( &elf_header.e_program_header_entries       , &offset );
	parseUint16 ( &elf_header.e_section_header_size          , &offset );
	parseUint16 ( &elf_header.e_section_header_entries       , &offset );
	parseUint16 ( &elf_header.e_string_table_section         , &offset );


	////////////////////////                           ////////////////////////
	////////////////////////  Parse the Segment Table  ////////////////////////
	////////////////////////                           ////////////////////////

	// Parse the section headers into a linked list.
	//
	// This should work for both 64 and 32-bit code
	//
	// If there are no entries, set the head pointer to NULL
	elf_section_list_head  = NULL;

	Elf_section_node** previousSection     = &elf_section_list_head ;
	Elf_section*       ptrToSectionInMmap  = (Elf_section*) (buf + elf_header.e_section_header_table_offset);
	int                index               = 0;

	//printf("Buf = [%p]\n", buf );
	//printf("Section header table offset = [%lx]\n", elf_header.e_section_header_table_offset );
	//printf("Section heaader size = [%x]\n", elf_header.e_section_header_size );

	while( index < elf_header.e_section_header_entries ) {
		// printf("Current offset = [%p]\n", ptrToSectionInMmap );

		Elf_section_node* newSection = (Elf_section_node*) malloc (sizeof ( Elf_section_node ));

		// Populate the new node
		newSection->elf_section = ptrToSectionInMmap;
		newSection->index = index;
		newSection->name = NULL;  // NULL by default
		newSection->next = NULL;

		// Check to see if it's a section name string table
		if( elf_header.e_string_table_section != SHN_UNDEF && index == elf_header.e_string_table_section ) {
			// printf( "Found string table as index [%d]\n", index );  // @todo remove (all printfs) before flight
			ptrToTextSectionNode = newSection;
		}

		// Increment
		index += 1;
		ptrToSectionInMmap = (Elf_section*)(((void*) ptrToSectionInMmap) + elf_header.e_section_header_size);
		*previousSection = newSection;
		previousSection = &(newSection->next);

		// printf( "index=[%d] \n", index );
	}

	// Traverse the linked list and set an easy-to-find pointer to all of the section names
	// We have to do this last because the string table section is usually at the end of the file
	// and we don't know its offset until we have parsed the entire table.
	Elf_section_node* currentNode = elf_section_list_head;
	char* name;

	if( ptrToTextSectionNode != NULL ) {
		while( currentNode != NULL ) {
			if( addressSize == ELF64 ) {
				name = buf + fixup64( ptrToTextSectionNode->elf_section->sh_offset ) + fixup32( currentNode->elf_section->sh_name );
			} else {
				name = buf + fixup32(((Elf_section32*)ptrToTextSectionNode->elf_section)->sh_offset ) + fixup32( currentNode->elf_section->sh_name );
			}
			// printf( "Name [%s]\n", name );
			currentNode->name = name;
			currentNode = currentNode->next;
		}
	}
}


void cleanupSectionList() {
	Elf_section_node* node = elf_section_list_head;

	while( node != NULL ) {
		Elf_section_node* nextNode = node->next;
		// printf( "Deleting [%s]\n", node->name );
		free( node );
		node = nextNode;
	}

	elf_section_list_head = NULL;
}
