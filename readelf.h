///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file readelf.h
/// @version 1.0 - Initial implementation of -h option
/// @version 2.0 - Refactored.  Added -t and -x options.
///
/// Display information about ELF files
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>
#include <inttypes.h>

extern const char    PROGRAM_NAME[];   ///< The name of this program... readelf
extern const uint8_t ELF_VERSION;      ///< Version of this elf parser (1.0)

typedef enum { ADDRESS_SIZE_UNKNOWN = -1 ,ELF32 = 0 ,ELF64 = 1 } Address_Size ;
typedef enum { ENDIAN_UNKNOWN       = -1 ,LITTLE    ,BIG       } Endian ;

extern Address_Size addressSize ;      ///< Enum for ELF32 or ELF64
extern Endian       elfEndianess ;     ///< Enum for LITTLE or BIG endian of the elf file
extern Endian       programEndianess ; ///< Enum for LITTLE or BIG endian of this program (readlef)
extern bool         isSameEndianess;   ///< Set to true if both the elf and this program
                                       ///< have the same endianess

#define SIZE_OF_IDENT_REGION (16)      ///< #define EI_NIDENT 16


//////////////////////////////                  //////////////////////////////
//////////////////////////////  ELF Structures  //////////////////////////////
//////////////////////////////                  //////////////////////////////

/// The 16 bytes of e_ident has some structure... Elf_magic breaks out
/// that structure.  It identifies an ELF file "7F ELF", the 32/64-bit
/// nature of the file and if you have a Big or Little Endian file.
typedef struct {
	unsigned char magic0;  // 0x7f
	unsigned char magic1;  // 'E'
	unsigned char magic2;  // 'L'
	unsigned char magic3;  // 'F'
	unsigned char e_class; // If 1, then 32-bit elf.  If 2, then 64-bit elf.
	unsigned char e_endianess;      // If 1, then little endian.  if 2, then big endian.
	unsigned char e_elf_version;    // The version number of the ELF specification
	unsigned char e_OS_ABI;         // The operating system and ABI for this elf file
	unsigned char e_OS_ABI_version; // The OS ABI version for this elf file
} Elf_magic;


/// This is a common structure for 32 and 64-bit elf files.  I expanded
/// some of the names to be more expressive.
///
/// There are 3 fields that are 32-bits or 64-bits depending on the
/// architecture:  e_entry, e_phoff and e_shoff.  For these fields,
/// we store the values in 64-bit fields.  The 32-bit elf files are
/// up-cast  to 64-bit fields.
typedef struct {
	union {
		unsigned char e_ident[SIZE_OF_IDENT_REGION];
		Elf_magic     elf_magic;
	} e_magic;
	uint16_t      e_type;
	uint16_t      e_machine;
	uint32_t      e_version;
	uint64_t      e_entry;
	uint64_t      e_program_header_table_offset;  // e_phoff
	uint64_t      e_section_header_table_offset;  // e_shoff;
	uint32_t      e_flags;
	uint16_t      e_elf_header_size;              // e_ehsize;
	uint16_t      e_program_header_entry_size;    // e_phentsize;
	uint16_t      e_program_header_entries;       // e_phnum;
	uint16_t      e_section_header_size;          // e_shentsize;
	uint16_t      e_section_header_entries;       // e_shnum;
	uint16_t      e_string_table_section;         // e_shstrndx;
} Elf_header;

extern Elf_header elf_header;


////////////////////////////                      ////////////////////////////
////////////////////////////  Segment Structures  ////////////////////////////
////////////////////////////                      ////////////////////////////


/// Define a 64-bit section structure
typedef struct {
	uint32_t   sh_name;
	uint32_t   sh_type;
	uint64_t   sh_flags;      // 32-bit on ELF32
	uint64_t   sh_addr;       // 32-bit on ELF32
	uint64_t   sh_offset;     // 32-bit on ELF32
	uint64_t   sh_size;       // 32-bit on ELF32
	uint32_t   sh_link;
	uint32_t   sh_info;
	uint64_t   sh_addralign;  // 32-bit on ELF32
	uint64_t   sh_entsize;    // 32-bit on ELF32
} Elf_section ;


/// Define a 32-bit section structure
typedef struct {
	uint32_t   sh_name;
	uint32_t   sh_type;
	uint32_t   sh_flags;      // 32-bit on ELF32
	uint32_t   sh_addr;       // 32-bit on ELF32
	uint32_t   sh_offset;     // 32-bit on ELF32
	uint32_t   sh_size;       // 32-bit on ELF32
	uint32_t   sh_link;
	uint32_t   sh_info;
	uint32_t   sh_addralign;  // 32-bit on ELF32
	uint32_t   sh_entsize;    // 32-bit on ELF32
} Elf_section32 ;


typedef struct Elf_section_node {
	Elf_section*             elf_section;  // Pointer directly into the memory mapped section table
	uint16_t                 index;        // 0, 1, 2, ...
	char*                    name;         // Reference to the section name in the string table
	struct Elf_section_node* next;         // Pointer to the next node
} Elf_section_node;

extern Elf_section_node* elf_section_list_head;

extern Elf_section_node* ptrToTextSectionNode;  /// Point to the node that holds
                                                /// a string table
