///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file print_header.c
/// @version 1.0 - Initial implementation of -h option
/// @version 2.0 - Refactored.  Added -t and -x options.
///
/// Print output for the -h file header option
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "readelf.h"
#include "print_header.h"


/// Print the first 16 bytes of the file magic in hex
///   Magic:   7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00
void printMagic() {
	printf( "  Magic:   ");

	for( int i = 0 ; i < SIZE_OF_IDENT_REGION ; i++ ) {
		printf( "%02x ", *(unsigned char*) (elf_header.e_magic.e_ident+i) );
	}

	printf( "\n" );
}


/// Print the "file class":  If the file is a 32-bit or 64-bit elf file
///   Class:                             ELF64
void printClass() {
	if( addressSize      == ELF32 ) {
		printf( "  Class:                             ELF32\n" );
	}
	else if( addressSize == ELF64 ) {
		printf( "  Class:                             ELF64\n" );
	}
	else {
		printf( "  Class:                             INVALID\n" );
	}
}


/// Print the Big Endian or Little Endian nature of the elf file
///   Data:                              2's complement, little endian
void printEndianess() {
	if( elfEndianess == LITTLE ) {
		printf( "  Data:                              2's complement, little endian\n" );
	}
	else if( elfEndianess == BIG ) {
		printf( "  Data:                              2's complement, big endian\n" );
	}
	else {
		printf( "  Data:                              UNKNOWN\n" );
	}
}


/// Print the version of this elf file
///   Version:                           1 (current)
void printVersion() {
	const unsigned char elfVersion = elf_header.e_magic.elf_magic.e_elf_version;
	printf( "  Version:                           %x", elfVersion );

	if( elfVersion == ELF_VERSION ) {
		printf( " (current)");
	}

	printf( "\n");
}


/// Print the the operating system and Application Binary Interface (ABI) to
/// which the object is targeted.
///   OS/ABI:                            UNIX - System V
void printABI() {
	const unsigned char elf_OS_ABI = elf_header.e_magic.elf_magic.e_OS_ABI;

	// This list comes from elf.h
	const unsigned char ELFOSABI_NONE       = 0   ;    /* UNIX System V ABI */
	const unsigned char ELFOSABI_SYSV       = 0   ;    /* Alias.  */
	const unsigned char ELFOSABI_HPUX       = 1   ;    /* HP-UX */
	const unsigned char ELFOSABI_NETBSD     = 2   ;    /* NetBSD.  */
	const unsigned char ELFOSABI_GNU        = 3   ;    /* Object uses GNU ELF extensions.  */
	const unsigned char ELFOSABI_LINUX      = ELFOSABI_GNU; /* Compatibility alias.  */
	const unsigned char ELFOSABI_SOLARIS    = 6   ;    /* Sun Solaris.  */
	const unsigned char ELFOSABI_AIX        = 7   ;    /* IBM AIX.  */
	const unsigned char ELFOSABI_IRIX       = 8   ;    /* SGI Irix.  */
	const unsigned char ELFOSABI_FREEBSD    = 9   ;    /* FreeBSD.  */
	const unsigned char ELFOSABI_TRU64      = 10  ;    /* Compaq TRU64 UNIX.  */
	const unsigned char ELFOSABI_MODESTO    = 11  ;    /* Novell Modesto.  */
	const unsigned char ELFOSABI_OPENBSD    = 12  ;    /* OpenBSD.  */
	const unsigned char ELFOSABI_ARM_AEABI  = 64  ;    /* ARM EABI */
	const unsigned char ELFOSABI_ARM        = 97  ;    /* ARM */
	const unsigned char ELFOSABI_STANDALONE = 255 ;    /* Standalone (embedded) application */

	if(      elf_OS_ABI == ELFOSABI_SYSV ) {
		printf( "  OS/ABI:                            UNIX - System V\n" );
	}
	else if( elf_OS_ABI == ELFOSABI_HPUX ) {
		printf( "  OS/ABI:                            UNIX - HP-UX\n" );
	}
}


/// Print the version of the ABI to which the object is targeted.
///   ABI Version:                       0
void printABIversion() {
	const unsigned char elf_OS_ABI_version = elf_header.e_magic.elf_magic.e_OS_ABI_version;

	printf( "  ABI Version:                       %x\n", elf_OS_ABI_version );
}


/// Print the type of ELF file... Executable, core, shared library, etc.
///   Type:                              EXEC (Executable file)
void printType() {
	const uint16_t elfType = elf_header.e_type;

	const uint16_t ET_NONE   = 0      ;
	const uint16_t ET_REL    = 0      ;
	const uint16_t ET_EXEC   = 2      ;
	const uint16_t ET_DYN    = 3      ;
	const uint16_t ET_CORE   = 4      ;
	const uint16_t ET_LOPROC = 0xff00 ;
	const uint16_t ET_HIPROC = 0xffff ;

	if(      elfType == ET_EXEC ) {
		printf( "  Type:                              EXEC (Executable file)\n" );
	}
	else if( elfType == ET_DYN ) {
		printf( "  Type:                              DYN (Shared object file)\n" );
	}
}


/// Print the required architecture for an elf file.
///   Machine:                           Advanced Micro Devices X86-64
void printMachine() {
	const uint16_t elfMachine = elf_header.e_machine;

	const uint16_t EM_NONE    =   0 ;
	const uint16_t EM_M32     =   1 ;
	const uint16_t EM_SPARC   =   2 ;
	const uint16_t EM_386     =   3 ;
	const uint16_t EM_68K     =   4 ;
	const uint16_t EM_88K     =   5 ;
	const uint16_t EM_860     =   7 ;
	const uint16_t EM_MIPS	  =   8 ;
	const uint16_t EM_PPC64   =  21 ;
	const uint16_t EM_S390    =  22 ;
	const uint16_t EM_ARM     =  40 ;
	const uint16_t EM_X86_64  =  62 ;
	const uint16_t EM_AARCH64 = 183 ;

	// printf( "Machine = [%hd]\n", inMachine );

	if(      elfMachine == EM_386 ) {
		printf( "  Machine:                           Intel 80386\n" );
	}
	else if( elfMachine == EM_X86_64 ) {
		printf( "  Machine:                           Advanced Micro Devices X86-64\n" );
	}
	else if( elfMachine == EM_AARCH64 ) {
		printf( "  Machine:                           AArch64\n" );
	}
	else if( elfMachine == EM_PPC64 ) {
		printf( "  Machine:                           PowerPC64\n" );
	}
	else if( elfMachine == EM_S390 ) {
		printf( "  Machine:                           IBM S/390\n" );
	}
	else if( elfMachine == EM_ARM ) {
		printf( "  Machine:                           ARM\n" );
	}
	else {
		printf( "  Machine:                           Unknown\n" );
	}
}


/// Print the elf file header
///
void printElfFileHeader() {
	printf( "ELF Header:\n" );

	/// These headers require some translation
	printMagic();
	printClass();
	printEndianess();
	printVersion();
	printABI();
	printABIversion();
	printType();
	printMachine();

	/// These headers are pretty basic... just print the value in elf_header
	printf( "  Version:                           0x%"PRIx32"\n"                , elf_header.e_version );
	printf( "  Entry point address:               0x%"PRIx64"\n"                , elf_header.e_entry );
	printf( "  Start of program headers:          %"PRId64" (bytes into file)\n", elf_header.e_program_header_table_offset );
	printf( "  Start of section headers:          %"PRId64" (bytes into file)\n", elf_header.e_section_header_table_offset );
	printf( "  Flags:                             0x%"PRIx32"\n"                , elf_header.e_flags );
	printf( "  Size of this header:               %"PRId16" (bytes)\n"          , elf_header.e_elf_header_size );
	printf( "  Size of program headers:           %"PRId16" (bytes)\n"          , elf_header.e_program_header_entry_size );
	printf( "  Number of program headers:         %"PRId16"\n"                  , elf_header.e_program_header_entries );
	printf( "  Size of section headers:           %"PRId16" (bytes)\n"          , elf_header.e_section_header_size );
	printf( "  Number of section headers:         %"PRId16"\n"                  , elf_header.e_section_header_entries );
	printf( "  Section header string table index: %"PRId16"\n"                  , elf_header.e_string_table_section );
}
