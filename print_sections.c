///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file print_sections.c
/// @version 1.0
///
/// Print section headers
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "readelf.h"
#include "print_sections.h"
#include "util.h"


char* getSectionType( const uint32_t type ) {
	const uint32_t SHT_NULL           = 0          ;   /* Section header table entry unused */
	const uint32_t SHT_PROGBITS       = 1          ;   /* Program data */
	const uint32_t SHT_SYMTAB         = 2          ;   /* Symbol table */
	const uint32_t SHT_STRTAB         = 3          ;   /* String table */
	const uint32_t SHT_RELA           = 4          ;   /* Relocation entries with addends */
	const uint32_t SHT_HASH           = 5          ;   /* Symbol hash table */
	const uint32_t SHT_DYNAMIC        = 6          ;   /* Dynamic linking information */
	const uint32_t SHT_NOTE           = 7          ;   /* Notes */
	const uint32_t SHT_NOBITS         = 8          ;   /* Program space with no data (bss) */
	const uint32_t SHT_REL            = 9          ;   /* Relocation entries, no addends */
	const uint32_t SHT_SHLIB          = 10         ;   /* Reserved */
	const uint32_t SHT_DYNSYM         = 11         ;   /* Dynamic linker symbol table */
	const uint32_t SHT_INIT_ARRAY     = 14         ;   /* Array of constructors */
	const uint32_t SHT_FINI_ARRAY     = 15         ;   /* Array of destructors */
	const uint32_t SHT_PREINIT_ARRAY  = 16         ;   /* Array of pre-constructors */
	const uint32_t SHT_GROUP          = 17         ;   /* Section group */
	const uint32_t SHT_SYMTAB_SHNDX   = 18         ;   /* Extended section indeces */
	const uint32_t SHT_NUM            = 19         ;   /* Number of defined types.  */
	const uint32_t SHT_LOOS           = 0x60000000 ;   /* Start OS-specific.  */
	const uint32_t SHT_GNU_ATTRIBUTES = 0x6ffffff5 ;   /* Object attributes.  */
	const uint32_t SHT_GNU_HASH       = 0x6ffffff6 ;   /* GNU-style hash table.  */
	const uint32_t SHT_GNU_LIBLIST    = 0x6ffffff7 ;   /* Prelink library list */
	const uint32_t SHT_CHECKSUM       = 0x6ffffff8 ;   /* Checksum for DSO content.  */
	const uint32_t SHT_LOSUNW         = 0x6ffffffa ;   /* Sun-specific low bound.  */
	const uint32_t SHT_SUNW_move      = 0x6ffffffa ;
	const uint32_t SHT_SUNW_COMDAT    = 0x6ffffffb ;
	const uint32_t SHT_SUNW_syminfo   = 0x6ffffffc ;
	const uint32_t SHT_GNU_verdef     = 0x6ffffffd ;   /* Version definition section.  */
	const uint32_t SHT_GNU_verneed    = 0x6ffffffe ;   /* Version needs section.  */
	const uint32_t SHT_GNU_versym     = 0x6fffffff ;   /* Version symbol table.  */
	const uint32_t SHT_HISUNW         = 0x6fffffff ;   /* Sun-specific high bound.  */

	if( type == SHT_NULL           )  return "NULL            ";
	if( type == SHT_PROGBITS       )  return "PROGBITS        ";
	if( type == SHT_SYMTAB         )  return "SYMTAB          ";
	if( type == SHT_STRTAB         )  return "STRTAB          ";
	if( type == SHT_RELA           )  return "RELA            ";
	if( type == SHT_HASH           )  return "HASH            ";
	if( type == SHT_DYNAMIC        )  return "DYNAMIC         ";
	if( type == SHT_NOTE           )  return "NOTE            ";
	if( type == SHT_NOBITS         )  return "NOBITS          ";
	if( type == SHT_REL            )  return "REL             ";
	if( type == SHT_SHLIB          )  return "SHLIB           ";
	if( type == SHT_DYNSYM         )  return "DYNSYM          ";
	if( type == SHT_INIT_ARRAY     )  return "INIT_ARRAY      ";
	if( type == SHT_FINI_ARRAY     )  return "FINI_ARRAY      ";
	if( type == SHT_PREINIT_ARRAY  )  return "PREINIT_ARRAY   ";
	if( type == SHT_GROUP          )  return "GROUP           ";
	if( type == SHT_SYMTAB_SHNDX   )  return "SYMTAB_SHNDX    ";
	if( type == SHT_NUM            )  return "NUM             ";
	if( type == SHT_LOOS           )  return "LOOS            ";
	if( type == SHT_GNU_ATTRIBUTES )  return "GNU_ATTRIBUTES  ";
	if( type == SHT_GNU_HASH       )  return "GNU_HASH        ";
	if( type == SHT_GNU_LIBLIST    )  return "GNU_LIBLIST     ";
	if( type == SHT_CHECKSUM       )  return "CHECKSUM        ";
	if( type == SHT_LOSUNW         )  return "LOSUNW          ";
	if( type == SHT_SUNW_move      )  return "SUNW_move       ";
	if( type == SHT_SUNW_COMDAT    )  return "SUNW_COMDAT     ";
	if( type == SHT_SUNW_syminfo   )  return "SUNW_syminfo    ";
	if( type == SHT_GNU_verdef     )  return "GNU_verdef      ";
	if( type == SHT_GNU_verneed    )  return "VERNEED         ";
	if( type == SHT_GNU_versym     )  return "VERSYM          ";
	if( type == SHT_HISUNW         )  return "HISUNW          ";

	return "                ";  // We get some unkown types (on ARM ELF files)
}


/// Sample 64-bit output:
/// There are 30 section headers, starting at offset 0x62d0:
///
/// Section Headers:
///   [Nr] Name
///        Type              Address          Offset            Link
///        Size              EntSize          Info              Align
///        Flags
///   [ 0]
///        NULL             0000000000000000  0000000000000000  0
///        0000000000000000 0000000000000000  0                 0
///        [0000000000000000]:
///
void print64_bitSectionDetail() {
	printf( "  [Nr] Name\n" );
	printf( "       Type              Address          Offset            Link\n" );
	printf( "       Size              EntSize          Info              Align\n" );
	printf( "       Flags\n" );

	Elf_section_node* current_section = elf_section_list_head;

	for( ; current_section != NULL ; current_section = current_section->next ) {
		// First line
		printf( "  [%2d] ", current_section->index );
		printf( "%s\n", current_section->name );

		// Second line
		printf( "       " );
		printf( "%s ", getSectionType( fixup32( current_section->elf_section->sh_type )));
		printf( "%016"PRIx64"  ", fixup64( current_section->elf_section->sh_addr ));
		printf( "%016"PRIx64"  ", fixup64( current_section->elf_section->sh_offset ));
		printf( "%"PRId32"\n",    fixup32( current_section->elf_section->sh_link ));

		// Third line
		printf( "       " );
		printf( "%016"PRIx64" ",  fixup64( current_section->elf_section->sh_size ));
		printf( "%016"PRIx64"  ", fixup64( current_section->elf_section->sh_entsize ));
		printf( "%-16"PRId32"  ", fixup32( current_section->elf_section->sh_info ));
		printf( "%"PRId64"\n",    fixup64( current_section->elf_section->sh_addralign ));

		// Fourth line
		printf( "       [%016"PRIx64"]: \n", fixup64( current_section->elf_section->sh_flags ));
	}

}


/// Sample 32-bit output:
/// There are 26 section headers, starting at offset 0x5350:
///
/// Section Headers:
///   [Nr] Name
///        Type            Addr     Off    Size   ES   Lk Inf Al
///        Flags
///   [ 0]
///        NULL            00000000 000000 000000 00   0   0  0
///        [00000000]:
///   [ 1] .interp
///        PROGBITS        00008154 000154 000013 00   0   0  1
///        [00000002]:
void print32_bitSectionDetail() {
	printf( "  [Nr] Name\n" );
	printf( "       Type            Addr     Off    Size   ES   Lk Inf Al\n" );
	printf( "       Flags\n" );

	Elf_section_node* current_section = elf_section_list_head;

	for( ; current_section != NULL ; current_section = current_section->next ) {
		Elf_section32* elf_section = (Elf_section32*) current_section->elf_section;

		// First line
		printf( "  [%2d] ", current_section->index );
		printf( "%s\n", current_section->name );

		// Second line
		printf( "       " );
		printf( "%s", getSectionType( fixup32( elf_section->sh_type )));
		printf( "%08"PRIx32" ", fixup32( elf_section->sh_addr ));
		printf( "%06"PRIx32" ", fixup32( elf_section->sh_offset ));
		printf( "%06"PRIx32" ", fixup32( elf_section->sh_size ));
		printf( "%02"PRIx32" ", fixup32( elf_section->sh_entsize ));
		printf( "%3"PRId32" ",  fixup32( elf_section->sh_link ));
		printf( "%3"PRId32" ",  fixup32( elf_section->sh_info ));
		printf( "%2"PRId32"\n", fixup32( elf_section->sh_addralign ));

		// Third line
		printf( "       " );
		printf( "[%08"PRIx32"]: \n", fixup32( elf_section->sh_flags ));
	}
}


void printElfFileSectionDetail() {
	printf( "There are %d section headers, starting at offset 0x%"PRIx64":\n"
		,elf_header.e_section_header_entries
		,elf_header.e_section_header_table_offset );

	printf( "\n" );
	printf( "Section Headers:\n" );

	if( addressSize == ELF64 ) {
		print64_bitSectionDetail();
	}

	if( addressSize == ELF32 ) {
		print32_bitSectionDetail();
	}
}
